-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th6 28, 2020 lúc 03:12 AM
-- Phiên bản máy phục vụ: 10.4.8-MariaDB
-- Phiên bản PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `thuvien`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bandoc`
--

CREATE TABLE `bandoc` (
  `id` int(11) NOT NULL,
  `mabd` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `tenbd` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `diachi` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `bandoc`
--

INSERT INTO `bandoc` (`id`, `mabd`, `tenbd`, `diachi`) VALUES
(6, '95624', 'SangPT', 'HN'),
(7, '49721', 'Hien CM', 'HN');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chuyennganh`
--

CREATE TABLE `chuyennganh` (
  `macn` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `tencn` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chuyennganh`
--

INSERT INTO `chuyennganh` (`macn`, `tencn`) VALUES
('55623', 'CNTT'),
('72673', 'CNPM'),
('86196', 'KHMT');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `muonsach`
--

CREATE TABLE `muonsach` (
  `id` int(11) NOT NULL,
  `bd_id` int(11) NOT NULL,
  `ngaymuon` date NOT NULL,
  `ngaytra` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `muonsach`
--

INSERT INTO `muonsach` (`id`, `bd_id`, `ngaymuon`, `ngaytra`) VALUES
(50681289, 6, '2020-01-02', '2020-12-02'),
(50681291, 7, '2000-02-02', '2021-02-02');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `muonsach_chitiet`
--

CREATE TABLE `muonsach_chitiet` (
  `id` int(11) NOT NULL,
  `muonsach_id` int(11) NOT NULL,
  `sach_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `muonsach_chitiet`
--

INSERT INTO `muonsach_chitiet` (`id`, `muonsach_id`, `sach_id`) VALUES
(1, 50681281, 2),
(3, 50681281, 1),
(4, 50681281, 3),
(18, 50681287, 1),
(21, 50681291, 5),
(22, 50681291, 6),
(23, 50681291, 5),
(24, 50681289, 6),
(25, 50681289, 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sach`
--

CREATE TABLE `sach` (
  `id` int(11) NOT NULL,
  `masach` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `tensach` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tacgia` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `namxb` int(4) DEFAULT NULL,
  `trangthai` int(11) NOT NULL COMMENT '1: da xuat ban; 0: chua xuat ban',
  `macn` char(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sach`
--

INSERT INTO `sach` (`id`, `masach`, `tensach`, `tacgia`, `namxb`, `trangthai`, `macn`) VALUES
(5, '64464', 'toi yeu viet nam', 'sangpt', 2020, 1, '86196'),
(6, '12119', 'Tieu doi xe khong kinh', 'Tokuda', 2020, 0, '72673'),
(7, '80665', 'Sang IT', 'ITDEV', 2020, 1, '55623');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bandoc`
--
ALTER TABLE `bandoc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `chuyennganh`
--
ALTER TABLE `chuyennganh`
  ADD PRIMARY KEY (`macn`);

--
-- Chỉ mục cho bảng `muonsach`
--
ALTER TABLE `muonsach`
  ADD PRIMARY KEY (`id`),
  ADD KEY `muonsach_bandoc_bd_id_fk` (`bd_id`);

--
-- Chỉ mục cho bảng `muonsach_chitiet`
--
ALTER TABLE `muonsach_chitiet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `muonsach_chitiet_ms_id_fk` (`muonsach_id`),
  ADD KEY `muonsach_chitiet_sach_id_fk` (`sach_id`);

--
-- Chỉ mục cho bảng `sach`
--
ALTER TABLE `sach`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sach_macn_fk` (`macn`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bandoc`
--
ALTER TABLE `bandoc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `muonsach`
--
ALTER TABLE `muonsach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50681292;

--
-- AUTO_INCREMENT cho bảng `muonsach_chitiet`
--
ALTER TABLE `muonsach_chitiet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT cho bảng `sach`
--
ALTER TABLE `sach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `muonsach`
--
ALTER TABLE `muonsach`
  ADD CONSTRAINT `muonsach_bandoc_bd_id_fk` FOREIGN KEY (`bd_id`) REFERENCES `bandoc` (`id`);

--
-- Các ràng buộc cho bảng `sach`
--
ALTER TABLE `sach`
  ADD CONSTRAINT `sach_macn_fk` FOREIGN KEY (`macn`) REFERENCES `chuyennganh` (`macn`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

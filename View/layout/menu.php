 <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">
  
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        chức năng chính
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Quản lý chuyên ngành</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Chức năng</h6>
            <a class="collapse-item" href="/QuanLyThuVien/View/quanlychuyennganh/index.php?hanhdong=them">thêm mới</a>
            <a class="collapse-item" href="/QuanLyThuVien/View/quanlychuyennganh/index.php?hanhdong=danhsach">danh sách</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-cog"></i>
          <span>Quản lý đầu sách</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Chức năng</h6>
            <a class="collapse-item" href="/QuanLyThuVien/View/quanlydausach/index.php?hanhdong=them">thêm mới</a>
            <a class="collapse-item" href="/QuanLyThuVien/View/quanlydausach/index.php?hanhdong=danhsach">danh sách</a>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#bandoc" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-cog"></i>
          <span>Quản lý bạn đọc</span>
        </a>
        <div id="bandoc" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Chức năng</h6>
            <a class="collapse-item" href="/QuanLyThuVien/View/quanlybandoc/index.php?hanhdong=them">thêm mới</a>
            <a class="collapse-item" href="/QuanLyThuVien/View/quanlybandoc/index.php?hanhdong=danhsach">danh sách</a>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#muonsach" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-cog"></i>
          <span>Quản lý mượn sách</span>
        </a>
        <div id="muonsach" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Chức năng</h6>
            <a class="collapse-item" href="/QuanLyThuVien/View/quanlymuonsach/index.php?hanhdong=them">thêm mới</a>
            <a class="collapse-item" href="/QuanLyThuVien/View/quanlymuonsach/index.php?hanhdong=danhsach">danh sách</a>
          </div>
        </div>
      </li>
    </ul>
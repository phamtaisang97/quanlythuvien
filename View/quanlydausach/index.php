<?php
include '../../database/config.php';
include '../../Controller/dauSachController.php';
if(isset($_GET['hanhdong'])){
    $hanhdong=$_GET['hanhdong'];
}else{
    $hanhdong=$_POST['hanhdong'];
}

switch ($hanhdong){
    case 'them':
        if(isset($_POST['hanhdong'])){
            $tensach = $_POST['tensach'];
            $masach =mt_rand(10000,99999);
            $tacgia = $_POST['tacgia'];
            $namxb = $_POST['namxb'];
            $trangthai = $_POST['trangthai'];
            $macn = $_POST['chuyennganh'];
            them($tensach, $masach, $tacgia, $namxb, $trangthai, $macn);
            redirect('index.php?hanhdong=danhsach');
        }
        $chuyennganh = lay_bang_chuyen_nganh();
        include 'create.php';
        break;
        
        
    case 'sua':
        $id = $_GET['id'];
        if(isset($_POST['hanhdong'])){
            $tensach = $_POST['tensach'];
            $tacgia = $_POST['tacgia'];
            $masach = $_POST['masach'];
            $namxb = $_POST['namxb'];
            $trangthai = $_POST['trangthai'];
            $macn = $_POST['chuyennganh'];
            sua($id, $masach, $tensach, $tacgia, $namxb, $trangthai, $macn);
            redirect('index.php?hanhdong=danhsach');
        }else{
            $sach = lay_sp_bang_id($id);
            $chuyennganh = lay_bang_chuyen_nganh();
            include 'edit.php';
        }
        break;
        
    case 'danhsach':
        $kqs = lay_tat_ca();
        include 'list.php';
        break;
    case 'xoa':
        $id = $_GET['id'];
        xoa($id);
        redirect('index.php?hanhdong=danhsach');
        break;
    default :
        redirect('index.php?hanhdong=danhsach');
        break;
        
}

?>